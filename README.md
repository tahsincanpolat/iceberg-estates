Bu proje emlak yönetim uygulaması olarak hazırlanmıştır. Projede randevu listeleme, randevu güncelleme, randevu oluşturma aktiviteleri vardır.
Randevu listele sayfasında randevular emlakçınun bulunduğu posta koduna göre listelenmekte, randevuler tarihe göre sıralanabilmekte ve randevu çalışanına
göre filtereleme yapılabilmektedir. Aynı zamanda tarihi geçmiş randevular belirtilmiştir. Tabloda listeli randevular randevu tarihine göre aktif ve pasif olarak belirtilmiştir. Randevu oluşturma sayfasında haritadan belirlediğimiz randevu oluşturma konumuna göre mesafe, seyahat süresi ve toplam randevu süresi hesaplanmıştır.
Projede VUE3 Composition API kullanılmıştır. İstekler için axios, tablo listelemeleri için vuetify, grid yapıları için bootstrap, store yönetimi için vuex kullanılmıştır.
## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
