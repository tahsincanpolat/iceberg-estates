
export default function mappingAppointmentUpdateRequest (source) {
    const data = {
        records:{
            id : source.value.id,
            fields: {
                appointment_date: source.value.fields.appointment_date,
                appointment_postcode: source.value.fields.appointment_postcode,
                contact_id: source.value.fields.contact_id,
                agent_id: source.value.fields.agent_id
             }
        }
        
    };
    const postData = {
        records: [
        {
            fields: {
                appointment_date: source.value.appointmentDate.value,
                appointment_postcode: source.value.postalCode.value,
                agent_id: [source.value.agent.value],
                contact_id: [source.value.contact.value],
            }
        }
    ]


}
return {data, postData} 
}
