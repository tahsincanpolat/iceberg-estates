import { reactive, computed } from 'vue';
import axios from 'axios'

const API_KEY = import.meta.env.VITE_API_KEY
const activeRequests = reactive([]);

export default function UsePageRequest(){
    const isLoading = computed(() => activeRequests.length > 0);
    const makeGetRequest = async (url) => {
        try {
          activeRequests.push(url);          
          const index = activeRequests.indexOf(url);
          const response = await axios({
            url:url,
            headers: {
              'Authorization': `Bearer ${API_KEY}`,
              'Content-Type': 'application/json',
            },            
          });

          if (index !== -1) {
            activeRequests.splice(index, 1);
          }
          const data = response.data.records;
          return data;
        } catch (error) {
          console.log(error)
        }
        finally{
          const index = activeRequests.indexOf(url);
          if (index !== -1) {
              activeRequests.splice(index, 1);
          }
        } 
    };
    const makePostRequest = async (url,payload) => {
      console.log(url)
      try {
        activeRequests.push(url);          
        const index = activeRequests.indexOf(url);      
        const response = await axios({
          url:url,
          method:"post",
          headers: {
            'Authorization': `Bearer ${API_KEY}`,
          },  
          data:payload          
        });

        if (index !== -1) {
          activeRequests.splice(index, 1);
        }
        const data = response.data.records;
        return data;
      } catch (error) {
        console.log(error)
      }
      finally{
        const index = activeRequests.indexOf(url);
        if (index !== -1) {
            activeRequests.splice(index, 1);
        }
      } 
  };
    const makeGetLocationPostCodeRequest = async (url) => {
      try {
        const response = await axios({
          url:url,         
        });
        const data = response.data.result;
        return data;
      } catch (error) {
        alert(error)
      }
  };
    const makeUpdateRequest = async (url,records) => {
       const headers = {
         'Authorization': `Bearer ${API_KEY}`,
       };
      
       axios.patch(url,records, { headers })
         .then(response => {
          const data = response.data.records;
          return data;
         })
         .catch(error => {
           console.error('Hata:', error);
         });
     
    };

    return { isLoading, makeGetRequest, makeUpdateRequest, makeGetLocationPostCodeRequest, makePostRequest };
}