import {ref} from 'vue'
import usePageRequests from './usePageRequest'

export default function useResource(resource){
  const { makeGetRequest, makeUpdateRequest, makeGetLocationPostCodeRequest, makePostRequest } = usePageRequests()
  const appointments = ref([])
  const agents = ref([])
  const appointment = ref(null)
  const contacts = ref([])
  const location = ref([])
  const postAppointment = ref([])

  const baseURL = `${import.meta.env.VITE_BASE_URL}/${resource}`
  const postCodeURL = `${import.meta.env.VITE_POST_CODE_BASE_URL}/${resource}`
  const fetchAllAppointents = async ()=> appointments.value = await makeGetRequest(baseURL)
  const fetchAllAgents = async ()=> agents.value = await makeGetRequest(baseURL)
  const fetchAllContacts = async ()=> contacts.value = await makeGetRequest(baseURL)
  const UpdateAppointment = async (payload,id) => appointment.value = await makeUpdateRequest(`${baseURL}`,payload)
  const fetchLocationPostCode = async (postcode)=> location.value = await makeGetLocationPostCodeRequest(`${postCodeURL}/${postcode}`)
  const PostAppointment = async (payload,id) => postAppointment.value = await makePostRequest(`${baseURL}`,payload)

  return {
    appointments,
    fetchAllAppointents,
    appointment,
    UpdateAppointment,
    agents,
    fetchAllAgents,
    fetchAllContacts,
    contacts,
    fetchLocationPostCode,
    location,
    PostAppointment,
    postAppointment
  }
}