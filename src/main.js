import { createApp } from 'vue'
import App from './App.vue'
import VueSplide from '@splidejs/vue-splide';
import router from './router'
import store from './store'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import VueGoogleMaps from '@fawmi/vue-google-maps'

import './styles/style.scss'
import 'bootstrap/dist/css/bootstrap.css';

const vuetify = createVuetify({
    components,
    directives,
})

const mapOption = {
    load: {
        apiKey: import.meta.env.VITE_MAP_API_KEY,
    }
}

const app = createApp(App)

app.use(router)
app.use(store)
app.use(VueSplide)
app.use(vuetify)
app.use(VueGoogleMaps, mapOption)


app.mount('#app')
