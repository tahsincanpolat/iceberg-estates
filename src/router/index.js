import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Appoinments from '../views/Appointments.vue'
import CreateAppoinment from '../views/CreateAppointment.vue'

const routes = [
  { path: "/", name: "Dashboard", component: Dashboard },
  { path: "/appointments", name: "Appoinments", component: Appoinments },
  { path: "/create-appointment", name: "CreateAppoinments", component: CreateAppoinment },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
