import { createStore } from 'vuex'

const state = {
    appointments: [] || null,
    agents: [] || null,
    contacts: [] || null,
    showUpdateModal: {
        show: false,
        payload: []
    }

}

const mutations = {
    setAppointments(state,appointments) {
        state.appointments = appointments
    },
    setAgents(state,agents) {
        state.agents = agents
    },
    setContacts(state,contacts) {
        state.contacts = contacts
    },
    setShowUpdateModal(state,value) {
        state.showUpdateModal.show = value.show,
        state.showUpdateModal.payload = value.payload
    }
}

const getters = {
    getAppointments: state => state.appointments.filter((appointment)=> appointment.fields.appointment_postcode === import.meta.env.VITE_POST_CODE),
    getAgents: state => state.agents,
    getContacts: state => state.contacts.filter((contact) => contact.fields.contact_name !== undefined && contact.fields.contact_surname !== undefined),
    getShowUpdateModal: state => state.showUpdateModal.show,
    getShowUpdateModalPayload: state => state.showUpdateModal.payload

}

const actions = {
    
}

export default createStore({
  state,
  getters,
  actions,
  mutations
})

